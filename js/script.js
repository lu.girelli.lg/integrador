$(document).ready(function() {
    

    (() => {
        'use strict'
        //objeto para guardar los datos del formulario
        class Formulario {
                nombre;
                apellido;
                email;
                telefono;
                documento;
                ciudad;
                provincia;
                nacimiento;
                curso;
                dias;
                turno;
                respuesta;
        }
        //nuevo objeto de la clase formulario
        let formConf = new Formulario()


        //validacion de los formularios
        const formPrimeraParte = $('#formPrimeraParte')
        const formSegundaParte = $('#formSegundaParte')
        const formTerceraParte = $('#formTerceraParte')
        const forms = document.querySelectorAll('.needs-validation')


        Array.from(forms).forEach(form => {
          form.addEventListener('submit', event => {
            if (!form.checkValidity()) {
              event.preventDefault()
              event.stopPropagation()
              form.classList.add('was-validated')
            } else {
                form.classList.add('was-validated')
                event.preventDefault()
                event.stopPropagation()
                
              
                //switch para mostrar/ocultar los formularios y mostrar alerta de mensaje enviado
                switch (form.id) {
                    case 'formPrimeraParte':
                        formPrimeraParte.addClass('d-none')
                        formSegundaParte.removeClass('d-none')
                        break;
                    case 'formSegundaParte':
                        llenarForm()
                        formSegundaParte.addClass('d-none')
                        formTerceraParte.removeClass('d-none')
                        break;
                    case 'formContacto':
                        $('#formContacto input').val('')
                        $('#formContacto textarea').val('')
                        form.classList.remove('was-validated')
                        appendAlert('Consulta enviada correctamente', 'success')
                        break;
                }

            }     

          }, false)
        })

      //boton atras segunda parte del formulario
      const botonAtras = $('#botonAtras')
      botonAtras.click(function() {
        formPrimeraParte.removeClass('d-none')
        formSegundaParte.addClass('d-none')
      })
      //boton volver tercera parte del formulario
      const botonVolver = $('#botonVolver')
      botonVolver.click(function() {
        formTerceraParte.addClass('d-none')
        formPrimeraParte.removeClass('d-none')
      })
      //boton enviar formulario, alerta de formulario enviado y reseteo de los formularios
      const botonEnviar = $('#botonEnviar')
      botonEnviar.click(function() {
        $('#formPrimeraParte input').val('')
        $('#formTerceraParte input').val('')
        document.getElementById('curso').selectedIndex = 0
        document.getElementById('dias').selectedIndex = 0
        formPrimeraParte.removeClass('was-validated')
        formSegundaParte.removeClass('was-validated')
        formTerceraParte.removeClass('was-validated')
        formTerceraParte.addClass('d-none')
        formPrimeraParte.removeClass('d-none')
        appendAlert2('Solicitud registrada exitosamente', 'success');
      })  



      function llenarForm () {
          formConf.nombre = $ ('#nombre').val()
          formConf.apellido = $ ('#apellido').val()
          formConf.email = $ ('#email').val()
          formConf.telefono = $ ('#telefono').val()
          formConf.documento = $ ('#documento').val()
          formConf.ciudad = $ ('#ciudad').val()
          formConf.provincia = $ ('#provincia').val()
          formConf.nacimiento = $ ('#nacimiento').val()
          formConf.curso = $('#curso option:selected').val()
          formConf.dias = $('#dias option:selected').val()
          formConf.turno = $('input[name="turno"]:checked').val()
          formConf.respuesta = $('input[name="respuesta"]:checked').val()

          $ ('#nombreConf').val( formConf.nombre)
          $ ('#apellidoConf').val(formConf.apellido)
          $ ('#emailConf').val(formConf.email)
          $ ('#telefonoConf').val(formConf.telefono)
          $ ('#documentoConf').val(formConf.documento)
          $ ('#ciudadConf').val(formConf.ciudad)
          $ ('#provinciaConf').val(formConf.provincia)
          $ ('#nacimientoConf').val(formConf.nacimiento)
          $ ('#cursoConf').val(formConf.curso)
          $ ('#diasConf').val(formConf.dias)
          $ ('#turnoConf').val(formConf.turno)
          $ ('#respuestaConf').val(formConf.respuesta)
      }

      const alertPlaceholder2 = document.getElementById('liveAlertPlaceholder2')
      const appendAlert2 = (message, type) => {
        const wrapper = document.createElement('div')
        wrapper.innerHTML = [
          `<div class="alert alert-${type} alert-dismissible" role="alert">`,
          `   <div>${message}</div>`,
          '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
          '</div>'
        ].join('')

        alertPlaceholder2.append(wrapper)
      }




      //alerta de mensaje enviado formulario contacto
      const alertPlaceholder = document.getElementById('liveAlertPlaceholder')
      const appendAlert = (message, type) => {
        const wrapper = document.createElement('div')
        wrapper.innerHTML = [
          `<div class="alert alert-${type} alert-dismissible" role="alert">`,
          `   <div>${message}</div>`,
          '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
          '</div>'
        ].join('')

        alertPlaceholder.append(wrapper)
      }

  })()

});

//exportar pdf
document.addEventListener("DOMContentLoaded", () => {
 
  const botonPdf = document.querySelector("#botonPdf");
  botonPdf.addEventListener("click", () => {

      const $elementoParaConvertir = document.querySelector("#formExport");
      
      
      html2pdf()
          .set({
              margin: 1,
              filename: 'documento.pdf',
              image: {
                  type: 'png',
                  quality: 0.98
              },
              html2canvas: {
                  scale: 3, 
                  letterRendering: true,
                  scrollY: 0,
              },
              jsPDF: {
                  unit: "in",
                  format: "a3", 
                  orientation: 'portrait' 
              }
          })
          .from($elementoParaConvertir)
          .save()
          .catch(err => console.log(err));
  });
});


//funcion para llamar api del clima
function clima () {
  let datos = false;
  let lat;
  let long;
  let temperatura;


  let settings = {
    "url" : "http://api.openweathermap.org/geo/1.0/direct?q="+"Buenos Aires"+
    "&limit=5&appid=302ddf5388fcad1d5158ff05d430a2a2",
    "method" : "GET",
    "timeout" : 0,
  };
  $.ajax(settings).done(function (response) {
    for (let key in response) {
      if (response[key].country == "AR") {
        datos = response[key];
        break;
      }
   }
  });

  setTimeout(function () {
    if (datos) {
      lat = datos.lat;
      long = datos.lon;

    $.ajax({
      url: `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&lang=es&appid=302ddf5388fcad1d5158ff05d430a2a2`,
      type: "GET",
    }) .done(function (data) {
      temperatura = Math.floor(data.main.temp - 273.1);
      $("#temp").text(temperatura + "°C");
      $("#iconoTemp").append("<img src='https://openweathermap.org/img/wn/"+data.weather[0].icon+".png'>");
      }).fail(function (error) {
        console.log(error);
      });
      }else {
        console.log("No se encontraron datos");
      }
    }, 2000);
 
};
